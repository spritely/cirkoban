;;; Copyright (C) 2024 David Thompson <dave@spritely.institute>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (game audio)
  #:use-module (dom element)
  #:use-module (dom media)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:export (make-sound-effect
            sound-effect?
            sound-effect-src
            play-sound-effect))

(define-record-type <sound-effect>
  (%make-sound-effect src elems)
  sound-effect?
  (src sound-effect-src)
  (elems sound-effect-elems))

(define max-simultaneous 8)

(define (make-sound-effect src)
  (let ((elems (make-vector max-simultaneous #f)))
    (vector-set! elems 0 (make-audio src))
    (%make-sound-effect src elems)))

(define* (play-sound-effect sound #:optional (volume 0.25))
  (define (play elem)
    (set-media-volume! elem volume)
    (media-play elem))
  (match sound
    (($ <sound-effect> src elems)
     (let lp ((i 0))
       (when (< i max-simultaneous)
         (match (vector-ref elems i)
           (#f
            (let ((elem (clone-element (vector-ref elems 0))))
              (vector-set! elems i elem)
              (play elem)))
           (elem
            (if (media-ended? elem)
                (play elem)
                (lp (1+ i))))))))))
