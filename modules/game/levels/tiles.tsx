<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.8.6" name="tiles" tilewidth="16" tileheight="16" tilecount="300" columns="20" objectalignment="topleft">
 <image source="../../../assets/images/cirkoban-onesheet.png" width="320" height="240"/>
 <tile id="2" type="wall">
  <properties>
   <property name="kind" value="copper"/>
  </properties>
 </tile>
 <tile id="14" probability="0.01"/>
 <tile id="15" probability="0.01"/>
 <tile id="16" probability="0.04"/>
 <tile id="17" probability="0.1"/>
 <tile id="18" probability="0.1"/>
 <tile id="19" probability="0.08"/>
 <tile id="27" type="exit"/>
 <tile id="30" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="80" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="81" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="82" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="83" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="84" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="85" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="86" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="87" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="100" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="101" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="102" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="103" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="104" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="105" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="106" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="107" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="120" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="121" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="122" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="123" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="124" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="125" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="126" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
 <tile id="127" type="wall">
  <properties>
   <property name="kind" value="inert"/>
  </properties>
 </tile>
</tileset>
