(define-module (srfi srfi-9)
  #:use-module ((scheme base) #:select (define-record-type))
  #:re-export (define-record-type))
