(define-module (srfi srfi-11)
  #:use-module ((hoot syntax) #:select (let-values let*-values))
  #:re-export (let-values let*-values))
