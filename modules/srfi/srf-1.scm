(define-module (srfi srfi-1)
  #:use-module ((scheme base) #:select (fold))
  #:re-export (fold))
