;;; Copyright (C) 2025 David Thompson <dave@spritely.institute>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (supporters)
  #:export (diamond-supporters
            gold-supporters
            silver-supporters))

(define diamond-supporters
  '("Aeva Palecek"
    "Daniel Finlay"
    "David Anderson"
    "Holmes Wilson"
    "Lassi Kiuru"
    "Péter Szilágyi"))

(define gold-supporters
  '("Adam Solove"
    "Alex Sassmannshausen"
    "Catherine Leonard"
    "Dave Hendler"
    "Juan Lizarraga Cubillos"))

(define silver-supporters
  '("Adam King"
    "Austin Robinson"
    "Brian Neltner"
    "Brit Butler"
    "Charlie McMackin"
    "Dan Connolly"
    "Danny OBrien"
    "Deb Nicholson"
    "Eric Schultz"
    "Evangelo Stavro Prodromou"
    "Evgeni Ku"
    "Glenn Thompson"
    "James Luke"
    "Jonathan Frederickson"
    "Joshua Simmons"
    "Justin Sheehy"
    "Keith Keydel"
    "Matthew Terenzio"
    "Michel Lind"
    "Mikayla Maki"
    "Moto A"
    "Nia Bickford"
    "Noah Beasley"
    "Shane Redman"
    "Steve Sprang"
    "Travis Smith"
    "Travis Vachon"
    "Zoé Cassiopée Gauthier"))
