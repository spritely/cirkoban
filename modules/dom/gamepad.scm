;;; Copyright (C) 2024 David Thompson <dave@spritely.institute>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;;
;;; Document bindings.
;;;
;;; Code:

(define-module (dom gamepad)
  #:use-module (hoot ffi)
  #:export (gamepad-index
            gamepad-button-ref

            gamepad-button-pressed?))

(define-foreign gamepad-index
  "gamepad" "index"
  (ref extern) -> i32)
(define-foreign gamepad-button-ref
  "gamepad" "button"
  (ref extern) i32 -> (ref null extern))

(define-foreign %gamepad-button-pressed?
  "gamepad" "buttonPressed"
  (ref extern) -> i32)
(define (gamepad-button-pressed? button)
  (eq? (%gamepad-button-pressed? button) 1))
