;;; Copyright (C) 2025 Amy Grinn <amy@spritely.institute>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;;
;;; HTML canvas TextMetrics interface bindings
;;;
;;; Code:

(define-module (dom text-metrics)
  #:pure
  #:use-module (scheme base)
  #:use-module (hoot ffi)
  #:export (text-metrics-actual-bounding-box-ascent
            text-metrics-actual-bounding-box-descent))

(define-foreign text-metrics-actual-bounding-box-ascent
  "textMetrics" "actualBoundingBoxAscent"
  (ref extern) -> i32)

(define-foreign text-metrics-actual-bounding-box-descent
  "textMetrics" "actualBoundingBoxDescent"
  (ref extern) -> i32)
