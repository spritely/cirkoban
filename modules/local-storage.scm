;;; Copyright (C) 2024 David Thompson <dave@spritely.institute>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (local-storage)
  #:use-module (hoot ffi)
  #:export (local-storage-ref
            local-storage-set!))

(define-foreign local-storage-ref
  "localStorage" "getItem"
  (ref string) -> (ref string))
(define-foreign local-storage-set!
  "localStorage" "setItem"
  (ref string) (ref string) -> none)
