(use-modules (guix git-download)
             (guix packages)
             (gnu packages base)
             (gnu packages compression)
             (gnu packages guile)
             (gnu packages guile-xyz))

(define guile-hoot*
  (let ((commit "db4f8f15c39535b89716e5e9c4a10abb63e51969"))
    (package
     (inherit guile-hoot)
     (version (string-append (package-version guile-hoot)
                             "-1." (string-take commit 7)))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.com/spritely/guile-hoot.git")
                    (commit commit)))
              (sha256
               (base32
                "16h7rjq1allg7ym6nxan5a6clq7x12ibb2z6bcln551gl5afzxvz"))))
     (arguments
      '(#:tests? #f)))))

(packages->manifest (list guile-next guile-hoot* gnu-make zip))
