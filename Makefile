modules = \
  modules/dom/canvas.scm \
  modules/dom/document.scm \
  modules/dom/element.scm \
  modules/dom/event.scm \
  modules/dom/gamepad.scm \
  modules/dom/image.scm \
  modules/dom/media.scm \
  modules/dom/window.scm \
  modules/game/actors.scm \
  modules/game/effects.scm \
  modules/game/level.scm \
  modules/game/particles.scm \
  modules/game/scripts.scm \
  modules/game/tileset.scm \
  modules/game/time.scm \
  modules/game/animation.scm \
  modules/goblins/abstract-types.scm \
  modules/goblins/core.scm \
  modules/goblins/core-types.scm \
  modules/goblins/ghash.scm \
  modules/guile/list.scm \
  modules/ice-9/control.scm \
  modules/ice-9/q.scm \
  modules/ice-9/vlist.scm \
  modules/local-storage.scm \
  modules/math.scm \
  modules/math/rect.scm \
  modules/math/vector.scm \
  modules/srfi/srfi-9.scm \
  modules/srfi/srfi-11.scm

levels = \
  modules/game/levels/tutorial-1.scm \
  modules/game/levels/tutorial-2.scm \
  modules/game/levels/tutorial-3.scm \
  modules/game/levels/tutorial-4.scm \
  modules/game/levels/tutorial-5.scm \
  modules/game/levels/tutorial-6.scm \
  modules/game/levels/tutorial-7.scm \
  modules/game/levels/tutorial-8.scm \
  modules/game/levels/tutorial-9.scm \
  modules/game/levels/tutorial-10.scm \
  modules/game/levels/rat-1.scm \
  modules/game/levels/rat-2.scm \
  modules/game/levels/rat-3.scm \
  modules/game/levels/catboss-1.scm \
  modules/game/levels/catboss-2.scm \
  modules/game/levels/catboss-3.scm \
  modules/game/levels/credits.scm

game.wasm: game.scm $(modules) $(levels)
	guild compile-wasm -L modules -o $@ $<

$(levels): %.scm: %.tmx scripts/compile-map.scm
	mkdir -p modules/game/levels
	guile scripts/compile-map.scm $< > $@

HOOT_REFLECT_JS_DIR=`guile -c '(display (@ (hoot config) %reflect-js-dir))'`
HOOT_REFLECT_WASM_DIR=`guile -c '(display (@ (hoot config) %reflect-wasm-dir))'`

reflect.js:
	cp $(HOOT_REFLECT_JS_DIR)/reflect.js $@
reflect.wasm:
	cp $(HOOT_REFLECT_WASM_DIR)/reflect.wasm $@
wtf8.wasm:
	cp $(HOOT_REFLECT_WASM_DIR)/wtf8.wasm $@

serve: game.wasm reflect.js reflect.wasm wtf8.wasm
	guile -c '((@ (hoot web-server) serve))'

bundle: game.wasm
	rm cirkoban.zip || true
	zip cirkoban.zip -r assets/images/cirkoban-onesheet.png \
                            assets/sounds/*.wav \
                            assets/music/*.ogg \
                            assets/fonts/*.woff2 \
                            reflect.js \
                            game.js \
                            game.css \
                            reflect.wasm \
                            wtf8.wasm \
                            game.wasm \
                            index.html

clean:
	rm -f game.wasm game.zip $(levels)

.PHONY: serve bundle clean
